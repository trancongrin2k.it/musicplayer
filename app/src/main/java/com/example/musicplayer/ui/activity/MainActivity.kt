package com.example.musicplayer.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.musicplayer.R
import com.example.musicplayer.databinding.ActivityMainBinding
import com.example.musicplayer.model.Song
import com.example.musicplayer.utils.Status
import com.example.musicplayer.viewmodel.SongViewModel
import com.example.musicplayer.viewmodel.SongViewModelFactory
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth;

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfig: AppBarConfiguration

    private val viewModel: SongViewModel by viewModels {
        SongViewModelFactory(application)
    }

    companion object {
        private const val LOG = "TCR"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // Initialize Firebase Auth
        auth = Firebase.auth
        val toolBar = binding.toolbar
        setSupportActionBar(toolBar)

        val navHost = supportFragmentManager.findFragmentById(R.id.nav_host) as NavHostFragment
        navController = navHost.navController

        //Đồng cấp
        appBarConfig = AppBarConfiguration(
            setOf(
                R.id.homeFragment,
                R.id.searchFragment,
                R.id.rankFragment,
                R.id.libraryFragment,
            )
        )

        setupActionBarWithNavController(navController, appBarConfig)

        binding.apply {
            bnvMain.setupWithNavController(navController)
            navView.setupWithNavController(navController)
        }

        //INSERT to DB
        lifecycleScope.launch {

            viewModel.getSong().observe(this@MainActivity) {
                it?.let {
                    when (it.status) {
                        Status.SUCCESS -> {
                            val songs = it.data
                            Log.d(LOG, songs.toString())
                            for (i in 0..songs!!.size) {
                                val song = Song(
                                    songs[i].idSong,
                                    songs[i].nameSong,
                                    songs[i].urlImage,
                                    songs[i].urlSong,
                                    songs[i].genre,
                                    songs[i].musician,
                                    songs[i].singer,
                                    songs[i].album,
                                    songs[i].release,
                                    songs[i].duration,
                                    songs[i].views,
                                    songs[i].isOffline
                                )
                                viewModel.insertSongToDB(song)
                            }
                        }
                        Status.LOADING -> {
                            Log.d(LOG, "Loading")
                        }
                        Status.ERROR -> {
                            Log.d(LOG, "Error")
                        }
                    }
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}