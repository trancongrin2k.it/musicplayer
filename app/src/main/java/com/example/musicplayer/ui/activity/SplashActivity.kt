package com.example.musicplayer.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Window
import com.example.musicplayer.R
import com.example.musicplayer.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity() {
    companion object {
        private const val LOG = "TCR"
    }
    private lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //ẩn title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //ẩn action bar
        supportActionBar?.hide()
        Handler(Looper.getMainLooper()).postDelayed({
            //Handle API

            //Start Activity
            startActivity(Intent(this, MainActivity::class.java))

            //Tắt luôn splash
            finish()
        }, 1000L)
    }
}