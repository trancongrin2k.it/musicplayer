package com.example.musicplayer.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.musicplayer.utils.COL_CATEGORY
import com.example.musicplayer.utils.COL_SONG_ID
import com.example.musicplayer.utils.TABLE_SONG
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = TABLE_SONG)
data class Song(
    @PrimaryKey(autoGenerate = true)
    @SerializedName(COL_SONG_ID)
    val idSong: Int?,
    val nameSong: String?,
    val urlImage: String?,
    var urlSong: String,
    @SerializedName(COL_CATEGORY)
    val genre: String?,
    val musician: String?,
    val singer: String?,
    val album: String?,
    val release: Int?,
    val duration: String?,
    val views: Int,
    val isOffline: Boolean
) : Serializable