package com.example.musicplayer.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.musicplayer.database.MusicDatabase
import com.example.musicplayer.model.Song
import com.example.musicplayer.network.SongClient
import com.example.musicplayer.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SongViewModel(application: Application) : AndroidViewModel(application) {
    companion object {
        private const val TAG: String = "TCR"
    }

    private val dao = MusicDatabase.getInstance(application.applicationContext).musicDAO()

    //GET Song from API
    private suspend fun getSongFromAPI() = SongClient.invoke().getSong()

    fun getSong() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            val data = getSongFromAPI().body()?.songList
            emit(Resource.success(data = data))
        } catch (ex: Exception) {
            emit(Resource.error(data = null, message = ex.message ?: "Error !!"))
        }
    }

    //INSERT Song to Database
    private suspend fun insertSongDao(song: Song) = dao.insertSong(song)

    fun insertSongToDB(song: Song) = viewModelScope.launch {
        insertSongDao(song)
    }

}