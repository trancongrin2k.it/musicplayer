package com.example.musicplayer.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.musicplayer.database.MusicDAO
import com.example.musicplayer.database.MusicDatabase
import com.example.musicplayer.model.Song

class RankRepository(context: Context) {
    private var dao: MusicDAO = MusicDatabase.getInstance(context).musicDAO()

    fun getSongsOnline(): LiveData<MutableList<Song>> = dao.getSongsOnline(0)

}