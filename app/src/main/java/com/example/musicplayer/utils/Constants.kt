package com.example.musicplayer.utils

const val DATABASE_VERSION = 1
const val DATABASE_NAME = "ManagerMusicPlayer"
const val TABLE_SONG = "tb_song"
const val COL_SONG_ID = "id"
const val COL_CATEGORY = "category"
