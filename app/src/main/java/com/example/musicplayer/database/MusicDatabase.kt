package com.example.musicplayer.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.musicplayer.model.Song
import com.example.musicplayer.utils.DATABASE_NAME
import com.example.musicplayer.utils.DATABASE_VERSION


@Database(entities = [Song::class], version = DATABASE_VERSION, exportSchema = false)
abstract class MusicDatabase : RoomDatabase() {

    abstract fun musicDAO(): MusicDAO

    companion object {
        private var instance: MusicDatabase? = null

        //singleton pattern
        @Synchronized
        fun getInstance(ctx: Context): MusicDatabase {
            if (instance == null) {
                //data/data/package/database/TeaManager.db
                instance = Room.databaseBuilder(
                    ctx.applicationContext,
                    MusicDatabase::class.java,
                    DATABASE_NAME
                ).build()
            }
            return instance!!
        }
    }
}
