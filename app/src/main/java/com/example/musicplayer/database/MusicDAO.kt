package com.example.musicplayer.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.musicplayer.model.Song

@Dao
interface MusicDAO {
    //INSERT Thêm dữ liệu từ API vào Local DB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSong(song: Song)

    //SELECT song by id
    @Query("SELECT * FROM tb_song WHERE idSong=:id")
    fun getSongById(id: Int): LiveData<Song>

    //SELECT Song rank
    @Query("SELECT * FROM tb_song WHERE isOffline=:isOffline")
    fun getSongsOnline(isOffline: Int): LiveData<MutableList<Song>>
}